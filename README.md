### Hello there 👋

I am a Computer Science specialized and interested in metaheuristics, machine learning and AI among many other tech stuff.

Here you are going to find differents projects, from practicals of subjects of the degree in CS from some years ago, to personal projects.

My social networks are:

<a href="https://masto.es/@advy"><img align="left" alt="advy99's Mastodon" height="30px" src="https://joinmastodon.org/logos/logo-purple.svg" /></a>
  

<br/>


### 💾 Languages I know and use

[<img align="left" alt="C" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/The_C_Programming_Language_logo.svg/1024px-The_C_Programming_Language_logo.svg.png"/>](https://en.wikipedia.org/wiki/C_(programming_language))
[<img align="left" alt="C++" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/1200px-ISO_C%2B%2B_Logo.svg.png"/>](https://en.wikipedia.org/wiki/C%2B%2B)
[<img align="left" alt="Python" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Python_logo_and_wordmark.svg/1920px-Python_logo_and_wordmark.svg.png"/>](https://en.wikipedia.org/wiki/Python_(programming_language))
[<img align="left" alt="Java" height="30px" src="https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/800px-Java_programming_language_logo.svg.png"/>](https://en.wikipedia.org/wiki/Java_(programming_language))
[<img align="left" alt="LaTeX" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Ruby_logo.svg/1024px-Ruby_logo.svg.png"/>](https://en.wikipedia.org/wiki/Ruby_(programming_language))
[<img align="left" alt="Ruby" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/LaTeX_project_logo_bird.svg/1920px-LaTeX_project_logo_bird.svg.png"/>](https://en.wikipedia.org/wiki/LaTeX)
[<img align="left" alt="Shell" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Bash_Logo_Colored.svg/512px-Bash_Logo_Colored.svg.png"/>](https://en.wikipedia.org/wiki/Bash_(Unix_shell))
[<img align="left" alt="CLIPS" height="30px" src="http://www.clipsrules.net/clipslogo.png"/>](https://en.wikipedia.org/wiki/CLIPS)


And many more ...

  
### 🚀 Technologies I like and use

[<img align="left" alt="Linux" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Tux.svg/800px-Tux.svg.png"/>](https://en.wikipedia.org/wiki/Linux)
[<img align="left" alt="CMake" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Cmake.svg/600px-Cmake.svg.png"/>](https://en.wikipedia.org/wiki/CMake)
[<img align="left" alt="CMake" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/d/d0/Meson_%28software%29_logo_2019.svg"/>](https://en.wikipedia.org/wiki/Meson_(software))
[<img align="left" alt="Vim" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Vimlogo.svg/1024px-Vimlogo.svg.png"/>](https://en.wikipedia.org/wiki/Vim_(text_editor))
[<img align="left" alt="Git" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/1280px-Git-logo.svg.png"/>](https://en.wikipedia.org/wiki/Git)
[<img align="left" alt="Markdown" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/1920px-Markdown-mark.svg.png"/>](https://en.wikipedia.org/wiki/Markdown)
[<img align="left" alt="OpenGL" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Opengl-logo.svg/120px-Opengl-logo.svg.png"/>](https://en.wikipedia.org/wiki/OpenGL)
[<img align="left" alt="SDL" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/1/16/Simple_DirectMedia_Layer%2C_Logo.svg"/>](https://en.wikipedia.org/wiki/Simple_DirectMedia_Layer)


<br/>

### My PC configuration

Actually I am using Arch Linux and my dotfiles are available [here](https://gitlab.com/advy99/dotfiles)
